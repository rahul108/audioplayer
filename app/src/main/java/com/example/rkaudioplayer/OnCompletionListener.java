package com.example.rkaudioplayer;

public interface OnCompletionListener {
    void onComplete();
}
